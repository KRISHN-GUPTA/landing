django~=1.10.5
django-crontab~=0.7.1
djangorestframework~=3.5.3
django-cors-headers~=1.3.1
django-common-helpers~=0.9.0
git+https://gitlab.com/sks444/igitt-django.git@sqlite3
# pip isn't installing the desired version with the ~= syntax,
# it is installing the old version instead.
IGitt==0.4.1.dev20180607094137
gunicorn~=19.6.0
requests~=2.12.4
django-brake~=1.5.2
PyGithub~=1.38
